package com.galvanize;

import org.apache.commons.lang3.StringUtils;
import lombok.Lombok;



public class Main {

    public static void main(String [] args) {


        if (StringUtils.equals(args[0], null))
        {
            System.out.println("Hello World");
        }else{
            System.out.println("Hello " + args[0]);
        }

    }
}